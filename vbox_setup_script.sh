set -e

# mkdir ~/Downloads
cd ~/Downloads
curl -O https://repo.anaconda.com/archive/Anaconda3-5.2.0-Linux-x86_64.sh
mkdir ~/.conda
bash Anaconda3-5.2.0-Linux-x86_64.sh -b -p $HOME/anaconda3
echo "export PATH=$HOME/anaconda3/bin:$PATH" >> $HOME/.profile
cd ~
source ~/.profile

apt install --assume-yes default-jdk
echo "export JAVA_HOME=/usr/lib/jvm/default-java" >> $HOME/.profile
source ~/.profile

pip install --user msgpack
pip install --user javabridge
pip install --user bioformats
echo "export PATH=$HOME/.local/bin:$PATH" >> $HOME/.profile
source ~/.profile

apt install --assume-yes ants

git clone https://github.com/flatironinstitute/CaImAn
cd CaImAn/
conda env create -f environment.yml -n caiman
source activate caiman
pip install .
